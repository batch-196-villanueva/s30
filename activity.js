db.fruits.aggregate([
    {$match: {$and:[{supplier:"Yellow Farms"},{price:{$lt:50}}]}},
    {$count: "yellowFarmsItemsPriceLessThan50"}
])

db.fruits.aggregate([
    {$match: {price:{$lt:30}}},
    {$count: "priceLessthan30"}
])

db.fruits.aggregate([
    {$match: {supplier:"Yellow Farms"}},
    {$group: {_id:"priceYF",avgPrice:{$avg:"$price"}}}
])

db.fruits.aggregate([
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"priceRFI",maxPrice:{$max:"$price"}}}
])

db.fruits.aggregate([
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"priceRFI",minPrice:{$min:"$price"}}}
])